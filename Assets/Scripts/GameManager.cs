﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GameManager : MonoBehaviour
{
    public PlayerController player1;
    public Player2Controller player2;

    public TMP_Text player1Points;
    public TMP_Text player2Points;
    //public TMP_Text player3Points;
    //public TMP_Text player4Points;

    public GameObject player1VictoryScreen;
    public GameObject player2VictoryScreen;
    public GameObject DrawScreen;

    public float Timer;
    public float StartTimer;
    public TMP_Text TimerText;
    public TMP_Text StarterTimer;

    public TMP_Text player1Reload;
    public TMP_Text player2Reload;
    void Start()
    {
        player1 = GameObject.FindObjectOfType<PlayerController>();
        player2 = GameObject.FindObjectOfType<Player2Controller>();

        player1VictoryScreen.gameObject.SetActive(false);
        player2VictoryScreen.gameObject.SetActive(false);
        DrawScreen.gameObject.SetActive(false);

        Timer = 60;
        StartTimer = 3;

        player1.enabled = true;
        player2.enabled = true;
    }

 
    void Update()
    {
        player1Points.text = "Points: " + player1.points;
        player2Points.text = "Points: " + player2.points;

        player1Reload.text = "Reload: " + player1.BulletReload.ToString("0");
        player2Reload.text = "Reload: " + player2.BulletReload.ToString("0");

        if (StartTimer <= 0)
        {
            player1.enabled = true;
            player2.enabled = true;

            Timer -= Time.deltaTime;

            StarterTimer.gameObject.SetActive(false);
        }
        else
        {
            StartTimer -= Time.deltaTime;
            player1.enabled = false;
            player2.enabled = false;
        }

        StarterTimer.text = StartTimer.ToString("0");

        TimerText.text = "Time left: " + Timer.ToString("0");

        if (Timer <= 0)
        {
            Time.timeScale = 0;
            ComparePoints();
        }
    }

    void ComparePoints()
    {
        if (player1.points > player2.points)
        {
            player1.enabled = false;
            player2.enabled = false;

            player1VictoryScreen.gameObject.SetActive(true);
        }

        if (player2.points > player1.points)
        {
            player1.enabled = false;
            player2.enabled = false;

            player2VictoryScreen.gameObject.SetActive(true);
        }

        if (player1.points == player2.points)
        {
            player1.enabled = false;
            player2.enabled = false;

            DrawScreen.gameObject.SetActive(true);
        }
    }
}
