﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controller : MonoBehaviour
{
    private Rigidbody rb;

    public GameObject Planet;

    public float speed;
    public float rotationSpeed;
    public float jumpHeight;

    public float gravity;
    bool TouchingFloor = false;
    public float DistancetoFloor;
    Vector3 Floornormal;

    public GameObject Bullet;
    public GameObject Cannon;
    public float firePower;

    public int points;

    public float BulletReload;

    public PlayerController player1;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;

        points = 0;

        BulletReload = 0;

        player1 = GameObject.FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        BulletReload -= Time.deltaTime;

        if (BulletReload < 0)
        {
            BulletReload = 0;
        }

        //Movement

        if (Input.GetKey(KeyCode.T))
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.G))
        {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }

        if (Input.GetKey(KeyCode.H))
        {
            transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
        }

        if (Input.GetKey(KeyCode.F))
        {
            transform.Rotate(0, -rotationSpeed * Time.deltaTime, 0);
        }

        //Jump
        if (Input.GetKeyDown(KeyCode.V))
        {
            rb.AddForce(transform.up * 40000 * jumpHeight * Time.deltaTime);
        }

        //Shot
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (BulletReload == 0)
            {
                GameObject bullet = Instantiate(Bullet);
                Rigidbody rbBullet = bullet.GetComponent<Rigidbody>();

                bullet.transform.position = Cannon.transform.position;
                rbBullet.AddForce(Cannon.transform.right * firePower, ForceMode.Impulse);

                Destroy(bullet, 5);

                BulletReload = 3;
            }
        }


        //GroundController
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up, out hit, 10))
        {
            DistancetoFloor = hit.distance;
            Floornormal = hit.normal;

            if (DistancetoFloor <= 0.2f)
            {
                TouchingFloor = true;
            }
            else
            {
                TouchingFloor = false;
            }
        }

        //Planet Controller
        Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;

        if (TouchingFloor == false)
        {
            rb.AddForce(gravDirection * -gravity);
        }

        Quaternion toRotation = Quaternion.FromToRotation(transform.up, Floornormal) * transform.rotation;
        transform.rotation = toRotation;

    }

    //When hit other Collider, change Planet
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform != Planet.transform)
        {
            Planet = collision.transform.gameObject;
            Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;

            Quaternion toRotation = Quaternion.FromToRotation(transform.up, gravDirection) * transform.rotation;
            transform.rotation = toRotation;

            rb.velocity = Vector3.zero;
            rb.AddForce(gravDirection * gravity);

        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("1Bullet"))
        {

            player1.points++;

        }
    }
}
