﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Rigidbody))]
public class BulletController : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (this.gameObject.CompareTag("1Bullet") && other.gameObject.CompareTag("Player2"))
        {
            Destroy(gameObject);
        }

        if (this.gameObject.CompareTag("2Bullet") && other.gameObject.CompareTag("Player1"))
        {
            Destroy(gameObject);
        }

        //if (this.gameObject.CompareTag("1Bullet") && other.gameObject.CompareTag("Player1"))
        //{
        //    Destroy(gameObject);
        //}

        //if (this.gameObject.CompareTag("2Bullet") && other.gameObject.CompareTag("Player2"))
        //{
        //    Destroy(gameObject);
        //}

        //if (this.gameObject.CompareTag("3Bullet") && other.gameObject.CompareTag("Player3"))
        //{
        //    Destroy(gameObject);
        //}

        //if (this.gameObject.CompareTag("4Bullet") && other.gameObject.CompareTag("Player4"))
        //{
        //    Destroy(gameObject);
        //}
    }
}
